export class GraphData {
  constructor (
  public january: number,
  public february: number,
  public march: number,
  public april: number,
  public may: number,
  public june: number,
  public july: number,
  public august: number,
  public september: number,
  public october: number,
  public november: number,
  public december: number
  ) {}
}
