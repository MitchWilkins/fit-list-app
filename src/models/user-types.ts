export class UserModel implements UserInterface {
  constructor(public id: string, public firstName: string, public lastName: string) {
  }
}

export class InstructorModel implements UserInterface {
  constructor(public id: string, public firstName: string, public lastName: string) {
  }
}

export interface UserInterface {
  id: string;
  firstName: string;
  lastName: string;
}
