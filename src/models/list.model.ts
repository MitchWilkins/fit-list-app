export class ListModel {
  constructor(public Id: string, public title: string, public complete: boolean, public completedOn: number, public createdOn: number) {}
}
