import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'completePipe'
})
export class CompletePipePipe implements PipeTransform {
  transform(value, filters) {
    const filter = (obj, filters) => {
      return Object.keys(filters).every(prop => obj[prop] === filters[prop]);
    };
    return value.filter(obj => filter(obj, filters[0]));
  }
}
