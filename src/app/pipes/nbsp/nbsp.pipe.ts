import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nbsp'
})
export class NbspPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.replace(/\s/g, '\u00A0');
  }

}
