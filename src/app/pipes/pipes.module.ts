import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompletePipePipe } from './complete/complete-pipe.pipe';
import { NbspPipe } from './nbsp/nbsp.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CompletePipePipe,
    NbspPipe
  ],
  exports: [
    CompletePipePipe,
    NbspPipe
  ]
})
export class PipesModule { }
