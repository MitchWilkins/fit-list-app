import { Component, OnInit } from '@angular/core';
import { CookieService } from '../services/cookie.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public username: string;
  constructor() { }

  ngOnInit() {
    this.username = CookieService.getCookie('userName');
  }
}
