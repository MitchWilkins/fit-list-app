import { Component, OnInit } from '@angular/core';
import { FireBaseService } from '../../services/fire-base.service';
import { Response } from '@angular/http';
import { InstructorModel, UserModel } from '../../../models/user-types';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.scss']
})
export class FollowingComponent implements OnInit {
  public following = [];
  public followerResults: InstructorModel[] = [];
  constructor(private fb: FireBaseService) { }

  ngOnInit() {
    /*    this.fb.getFollowing('mitchrwilkins')
          .subscribe((resp: Response) => {
            const data = resp.json();
            this.following.push(data);
          });*/
  }
  public searchData(query: any) {
    /*    this.fb.getInstructor()
          .subscribe((res: Response) => {
          const data = res.json();
          const dataArray = Object.keys(data);
            this.followerResults = [];
            for (let i = 0; i < dataArray.length; i++) {
              const instructorName = `${data[dataArray[i]]['firstName']} ${data[dataArray[i]]['lastName']}`;
              if (instructorName.indexOf(query) !== -1) {
                this.followerResults.push({
                  id: data[dataArray[i]]['id'],
                  firstName: data[dataArray[i]]['firstName'],
                  lastName: data[dataArray[i]]['lastName']
                });
              }
            }
          console.log(this.followerResults);
        });*/
  }

}
