import { Component, OnInit } from '@angular/core';
import { FireBaseService } from '../../services/fire-base.service';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
  public graphShow = false;
  public start = 'SELECT';
  public end = 'SELECT';
  private activeDataArray: Array<number> = [0];
  private completeDataArray: Array<number> = [0];
  private deletedDataArray: Array<number> = [0];
  private startIndex: number;
  private endIndex: number;
  // lineChart
  public lineChartData: Array <any> = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Active'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Complete'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Deleted'}
  ];
  public lineChartLabels: Array <any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
  'September', 'October', 'November', 'December'];
  public startChartLabels: Array <any> = this.lineChartLabels;
  public endChartLabels: Array <any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'];
  public newChartLabels: Array <any> = [];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array <any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  constructor(private fire: FireBaseService) {}

  ngOnInit() {
  }

  public randomize(): void {
    this.fire.getList()
      .subscribe((items: any) => {
        const listItems = Object.keys(items);
        listItems.map(x => this.fire.getListItems(x)
          .subscribe((workout) => {
            const convertMonth: any = (time: number) => {
              const numMonth: number = new Date(time).getMonth();
              console.log('numMonth ' + numMonth);
              return numMonth;
            };
            console.log(this.startIndex === convertMonth(workout.createdOn));
            if (convertMonth(workout.createdOn) >= this.startIndex && convertMonth(workout.createdOn) <= this.endIndex) {
              console.log('pass');
              if (workout.archived === true && workout.complete) {
                return this.deletedDataArray[convertMonth(workout.createdOn)] += 1;
              }
              if (!workout.complete) {
                return this.activeDataArray[convertMonth(workout.createdOn)] += 1;
              }
              if (workout.complete) {
                return this.completeDataArray[convertMonth(workout.completed)] += 1;
              }
            }
          })
        );
      });
    console.log(this.lineChartLabels.slice(this.startIndex, this.endIndex + 1), this.endChartLabels);
    this.newChartLabels = this.lineChartLabels.slice(this.startIndex, this.endIndex + 1);
    this.graphShow = true;
    const _lineChartData: Array <any> = new Array(3);
    const dataArrays = [this.activeDataArray, this.deletedDataArray, this.completeDataArray];
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: dataArrays[i], label: this.lineChartData[i].label};
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
  public startMonth(index: number) {
    this.start = this.lineChartLabels[index];
    this.startIndex = index;
  }
  public endMonth(index: number) {
    this.end = this.lineChartLabels[index];
    this.endIndex = index;
  }
}
