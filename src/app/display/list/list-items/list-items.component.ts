import { Component, OnInit } from '@angular/core';
import { FireBaseService } from '../../../services/fire-base.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ListModel } from '../../../../models/list.model';
import { Response } from '@angular/http';
import { WorkoutsService } from '../../../services/azure/workouts.service';
import { DateService } from '../../../services/service-extensions/date.service';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {
  public list: ListModel[] = [];
  public workoutTitle: string;
  private filterType: string;

  constructor(private fire: FireBaseService, private route: ActivatedRoute, private work: WorkoutsService) {
  }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.filterType = params['type'];
        return this.getWorkouts(this.filterType);
      });
  }

  public submitActivity(index: number, isCompleted: boolean) {
    this.work.putWorkout(this.list[index].Id, this.list[index].title, this.list[index].createdOn.toString(), !isCompleted);
    if (this.filterType !== 'all') {
      return this.list.splice(index, 1);
    } else {
      return !this.list[index].complete ? this.list[index].complete = true : this.list[index].complete = false;
    }
  }

  public submitWorkout(item: string) {
    this.work.postWorkout(item, this.list);
    this.workoutTitle = '';
  }

  private getWorkouts(type: string) {
    const filterFunc = (x) => {
      if (type === 'all' && !x.Archived) {
        return x;
      }
      if (type === 'complete' && !x.Archived) {
        return x.Completed;
      }
      if (type === 'active' && !x.Archived) {
        return !x.Completed;
      }
    };
    this.list = [];
    this.work.getWorkout('', this.list, filterFunc);
  }
  public createOrComplete(isCompleted: boolean, createdOn: number, completedOn: number): string {
    return !isCompleted ? `created: ${DateService.dateConvert(createdOn)}` : `completed ${DateService.dateConvert(completedOn)}`;
  }

  public deleteWorkout(i) {
    this.work.putWorkout(this.list[i].Id, this.list[i].title, this.list[i].createdOn.toString(), this.list[i].complete, true);
    this.list.splice(i, 1);
  }
}
