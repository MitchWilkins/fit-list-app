import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListComponent } from './display/list/list.component';
import { FireBaseService } from './services/fire-base.service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { PipesModule } from './pipes/pipes.module';
import { NavbarComponent } from './navbar/navbar.component';
import { ModalComponent } from './modal/modal.component';
import { DisplayComponent } from './display/display.component';
import { ActivityComponent } from './display/activity/activity.component';
import { ListItemsComponent } from './display/list/list-items/list-items.component';
import { ListMenuComponent } from './display/list/list-menu/list-menu.component';
import { ChartsModule } from 'ng2-charts';
import { AppRoutingModule } from './routes/app-routing.module';
import { FollowingComponent } from './display/following/following.component';
import { WorkoutsService } from './services/azure/workouts.service';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    NavbarComponent,
    ModalComponent,
    DisplayComponent,
    ActivityComponent,
    ListItemsComponent,
    ListMenuComponent,
    FollowingComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    PipesModule,
    AppRoutingModule,
    ChartsModule
  ],
  providers: [
    FireBaseService,
    WorkoutsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
