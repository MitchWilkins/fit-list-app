import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { DateService } from '../service-extensions/date.service';

@Injectable()
export abstract class AzureService {
  constructor(public http?: Http) {
  }

  private readonly endPoint: string = 'https://fitlist-api20180129125257.azurewebsites.net/api';
  protected nowDate = DateService.date;

  protected getModel(table: string, ID: string) {
    return this.http.get(`${this.endPoint}/${table}/${ID}`);
  }

  protected postModel(table: string, entry: any) {
    return this.http.post(`${this.endPoint}/${table}/`, entry);
  }

  protected putModel(ID: string, table: string, entry: any) {
    return this.http.put(`${this.endPoint}/${table}/${ID}`, entry);
  }
}
