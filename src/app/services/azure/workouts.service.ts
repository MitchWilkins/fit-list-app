import { Http, Response } from '@angular/http';
import { AzureService } from './azure.service';
import { ListModel } from '../../../models/list.model';
import { Injectable } from '@angular/core';
import { GuidService } from '../service-extensions/guid.service';
import { DateService } from '../service-extensions/date.service';

@Injectable()
export class WorkoutsService extends AzureService {
  constructor(public http: Http) {
    super();
  }

  public getWorkout(ID: string, list: ListModel[], filter?: any) {
    return this.getModel('Items', ID)
      .subscribe((res: Response) => {
        const data = res.json(),
          filterData = filter === undefined ? data : data.filter(x => filter(x));
        filterData.map(x => list.push({
          Id: x.ID,
          title: x.Description,
          complete: x.Completed,
          completedOn: x.CompletedOn,
          createdOn: x.CreatedOn
        }));
      });
  }

  public postWorkout(Description: string, list: ListModel[]) {
    const entry = {
      'ID': GuidService.newGuid(),
      'Description': Description,
      'Completed': false,
      'CompletedOn': DateService.dateFormat(0),
      'CreatedOn': DateService.dateFormat(this.nowDate),
      'Archived': false,
      'UserId': GuidService.UserId,
      'Type': 1
    };
    return this.postModel('Items', entry)
      .subscribe((res: Response) => {
          const data = res.json();
          list.push({
            Id: data.ID,
            title: data.Description,
            complete: data.Completed,
            completedOn: data.CompletedOn,
            createdOn: data.CreatedOn
          });
        }, err => console.log(err)
      );
  }

  public putWorkout(ID: string, Description: string, created: string, complete: boolean, isArchived: boolean = false) {
    const completed = complete ? DateService.dateFormat(this.nowDate) : DateService.dateFormat(0);
    const entry = {
      'ID': ID,
      'Description': Description,
      'Completed': complete,
      'CompletedOn': completed,
      'CreatedOn': created,
      'Archived': isArchived,
      'UserId': GuidService.UserId,
      'Type': 1
    };
    return this.putModel(ID, 'Items', entry)
      .subscribe((res: Response) => console.log(res.status), err => console.log('put error:\n' + err));
  }

}
