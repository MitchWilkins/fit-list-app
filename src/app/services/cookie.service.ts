export abstract class CookieService {
  public static getCookie(name: string) {
    const ca: Array<string> = document.cookie.split(';'),
      caLen: number = ca.length,
      cookieName = `${name}=`;
    let c: string;
    for (let i = 0; i < caLen; i += 1) {
      c = ca[i].replace(/^\s+/g, '');
      if (c.indexOf(cookieName) === 0) {
        return c.substring(0, cookieName.length);
      }
    }
    return '';
  }
}
