export class DateService {
  public static date: number = new Date().getTime();

  public static dateFormat = (milliseconds: number | string) => `/Date(${milliseconds}+0000)/`;

  public static fromSqlFormat(dateString: string) {
    const ds = dateString.replace(/\W|T/g, '/').replace('+0000', '');
    const dsArr = ds.split('/').map(x => +x);
    const dMill = new Date(dsArr[0], dsArr[1], dsArr[2], dsArr[3], dsArr[4], dsArr[5], dsArr[6]).getTime();
    return DateService.dateFormat(dMill);
  }

  public static dateConvert = (d) => {
    const dReplace = d.replace('/Date(', '').replace('+0000)/', '');
    const dTime = new Date(dReplace).toISOString();
    const dArray = dTime.replace('T', '-')
      .split('-');
    return `${dArray[1]}/${dArray[2]}/${dArray[0]}`;
  }
}
