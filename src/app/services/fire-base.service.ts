import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class FireBaseService {
  public d: number = new Date().getTime();
  constructor(public http: Http) { }
  public getList() {
    return this.http.get(`https://fitlist-fb-api.firebaseio.com/items.json`)
      .map((res: Response) => {
          return res.json();
        }
      );
  }
  public getListItems(workout: string) {
    return this.http.get(`https://fitlist-fb-api.firebaseio.com/items/${workout}.json`)
      .map((res: Response) => {
        return res.json();
        }
      );
  }
  public postListItem(Title: string) {
    const entry = {'title': Title, 'complete': false, 'completedOn': 0, 'createdOn': this.d};
    return this.http.post(`https://fitlist-fb-api.firebaseio.com/items.json`, entry);
  }
  public putCompletedItem(ID: string, Title: string, created: number, complete: boolean) {
    const completed = !complete ? this.d : 0;
    const entry = {'title': Title, 'complete': !complete, 'completedOn': completed, 'createdOn': created};
    return this.http.put(`https://fitlist-fb-api.firebaseio.com/items/${ID}.json`, entry)
      .subscribe((res: Response) => console.log(res.status), err => console.log('put error:\n' + err));
  }
  public deleteItem(ID: string) {
    return this.http.delete(`https://fitlist-fb-api.firebaseio.com/items/${ID}.json`)
      .subscribe((res: Response) => console.log(res.status), err => console.log('put error:\n' + err));
  }
}
