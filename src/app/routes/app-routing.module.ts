import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from '../display/list/list.component';
import { ListItemsComponent } from '../display/list/list-items/list-items.component';
import { FollowingComponent } from '../display/following/following.component';

const appRoutes: Routes = [
  {path: '', redirectTo: '/mylist/active', pathMatch: 'full'},
  {
    path: 'mylist', component: ListComponent, children: [
    {path: '', redirectTo: '/mylist/active', pathMatch: 'full'},
    {path: ':type', component: ListItemsComponent}
  ]
  },
  {path: 'following', component: FollowingComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
