// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCCUA8K1_oLUdmPQJSM6aT1G49NUhEXnAQ',
    authDomain: 'fitlist-fb-api.firebaseapp.com',
    databaseURL: 'https://fitlist-fb-api.firebaseio.com',
    projectId: 'fitlist-fb-api',
    storageBucket: 'fitlist-fb-api.appspot.com',
    messagingSenderId: '629164707643'
  }
};
